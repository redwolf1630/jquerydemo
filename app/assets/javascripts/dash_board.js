// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

function showWelcome()
{
    var welcome = document.getElementById('description');
    welcome.style.display = 'block';
}

function hideWelcome()
{
    var welcome = document.getElementById('description');
    welcome.style.display = 'none';
}